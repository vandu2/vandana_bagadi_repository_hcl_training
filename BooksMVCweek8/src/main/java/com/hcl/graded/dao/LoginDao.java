package com.hcl.graded.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.graded.beans.BookUsers;



public class LoginDao {
	
	
	@Autowired
	private JdbcTemplate template;



	public boolean validateUser(String username, String password) {
		String sql = "select username, password from BookUsers where username=? and password=?";
		List<BookUsers> user = template.query(sql, new LoginRowMapper(), username , password);
		if(user.size()==0)
			return false;
		else
			return true;
		
	}
}
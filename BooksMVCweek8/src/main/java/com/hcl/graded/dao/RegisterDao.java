package com.hcl.graded.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.graded.exception.UserException;



public class RegisterDao {

	@Autowired
	private JdbcTemplate template;


	public boolean registerUser(String username, String password) throws DuplicateKeyException{
		
		String sql = "insert into BookUsers values (?,?)";
		 if(template.update(sql, username, password)!=0)
			return true;
		else
			throw new DuplicateKeyException("Username Already Exists.! try with different Username");
		
		
	}
}
package com.hcl.graded.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.graded.beans.Books;
import com.hcl.graded.dao.BooksDao;

public class BooksService {

	@Autowired
	BooksDao dao;

	public List<Books> getAllBooks() {
		return dao.getAllBooks();

	}
}

package com.hcl.graded.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.hcl.graded.beans.LikedBooks;
import com.hcl.graded.dao.LikedDao;

public class LikedBooksService {

	@Autowired
	LikedDao dao;

	public List<LikedBooks> getAllBooks() {
		return dao.getAllBooks();
	}

	public boolean addBooks(int id, String name, String genre) throws DuplicateKeyException {
		return dao.addBooks(id, name, genre);
	}

	public boolean deleteBook(int id) {
		return dao.deleteBook(id);
	}
	
	
}

package com.hcl.graded.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.graded.dao.LoginDao;

public class LoginService {

	@Autowired
	LoginDao dao ;

	public boolean validateUser(String username, String password) {
		return dao.validateUser(username, password);
	}
}

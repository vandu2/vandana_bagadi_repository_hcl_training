package com.hcl.graded.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.hcl.graded.beans.ReadLaterBooks;
import com.hcl.graded.dao.ReadLaterDao;

public class ReadLaterService {

	@Autowired
	ReadLaterDao dao;

	public List<ReadLaterBooks> getAllBooks() {
		return dao.getAllBooks();
	}

	public boolean addBooks(int id, String name, String genre) throws DuplicateKeyException {
		return dao.addBooks(id, name, genre);
	}

	public boolean deleteBook(int id) {
		return dao.deleteBook(id);
	}
}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ page import="java.util.*"%>
	<%@ page import="com.hcl.graded.beans.*"%>
	<center>
		<pre>
			<h1>   ONLINE BOOKSTORE        <button
					onclick="location.href='login'">Login</button> <button
					onclick="location.href='register'">Register</button>
			</h1>
		</pre>
		<h4>READ BOOKS GAIN KNOWDLEDGE</h4>
	</center>
	<br />

	<hr>

	<br />
	<br />
	<center>
	Displaying All Available Books
	</center>

	<br />
	<br />
	<table border="2" width="70%" cellpadding = "2" align ="center">

		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Genre</b></th>
		</tr>


		<c:forEach var="i" items="${books}">
			<tr align = "center">
				<td><c:out value="${i.getId() }"></c:out></td>
				<td><c:out value="${i.getName() }"></c:out></td>
				<td><c:out value="${i.getGenre() }"></c:out></td>
			</tr>
		</c:forEach>

	</table>



</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liked Section</title>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<center>
		<h2>Liked Section</h2>
	</center>
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h4>Username :  " + username + " </h4><br/ >");
	%>
	<table border="2" width="70%" cellpadding = "2" align ="center">

		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Genre</b></th>
			<th></th>
			
		</tr>


		<c:forEach var="i" items="${books}">
			<tr align = "center">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getGenre() }"></c:out></td>
   					<td><a href="likedDelete/${i.getId()}">Delete</a></td>  
					
			</tr>
		</c:forEach>

	</table>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success</title>
</head>
<body>
<h1>Success.!</h1>
Hey, <strong> ${username} </strong> your account is successfully created. <br/> <br/>
You are now free to use. 
<a href ="login">Login </a>
</body>
</html>
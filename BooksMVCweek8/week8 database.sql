use bagadi;
create table BookUsers(username varchar(30) primary key,password varchar(30));


create table Books(id int primary key,name varchar(30),genre varchar(30));

insert into Books values('1001', 'The sound and the fury', 'Classics');
insert into Books values('1002', 'Catch-22',  'Horror');
insert into Books values('1003', 'darkness at noon',  'Romance');
insert into Books values('1004','sons and lovers','Autobiography');
insert into Books values('1005', 'under the volcano', 'Autobiography');
insert into Books values('1006', '1984', 'Sci-Fi');
insert into Books values('1007', 'the way of all flesh',  'Political Satire');
insert into Books values(1008, 'to the lighthouse',  'Self-Help');
insert into Books values(1009, 'claudius', 'Motivational');
insert into Books values('1010', 'A passage to India','Self-Help');




create table likelist(id int primary key,name varchar(30),genre varchar(30));
create table readlater(id int primary key,name varchar(30),genre varchar(30));
commit;
package com.greatlearning.hcl.JavaMiniProject;
import java.time.LocalDateTime;

import java.util.Date;
import java.util.List;
 class BillInformation {
		private String User;
		private List<Menu> itemsOrdered;
		private double Amount;
		private Date time;
		
		public BillInformation() {}
		
		public BillInformation(String name, List<Menu> items, double cost, Date time) throws IllegalArgumentException {
			super();
			
			this.User = name;
			this.itemsOrdered = items;
			if(Amount<0)
			{
				throw new IllegalArgumentException("exception occured, Bill cannot be less than zero");
			}
			this.Amount = cost;
			this.time = time;
		}
		
		public String getUser() {
			return User;
		}
		public void setUser(String name) {
			this.User = name;
		}
		
		public List<Menu> getItems() {
			return itemsOrdered;
		}
		public void setItems(List<Menu> OrderedItems) {
			this.itemsOrdered = OrderedItems;
		}
		public double getAmount() {
			return Amount;
		}
		public void setAmount(double price) {
			this.Amount = price;
		}
		
		public Date getTime() {
			return time;
		}
		public void setTime(Date date) {
			this.time = date;
		}
	}
		
		







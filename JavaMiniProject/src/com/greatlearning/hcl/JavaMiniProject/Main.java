package com.greatlearning.hcl.JavaMiniProject;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		Date time=new Date();
		
		List<BillInformation> totalbill = new ArrayList<BillInformation>();
		boolean finalOrder;

		List<Menu> items = new ArrayList<Menu>();

		Menu m1 = new Menu(1, "Rajma chaval",2, 150.0);
		Menu m2 = new Menu(2, "momos", 2, 190.0);
		Menu m3 = new Menu(3, "Red thai Curry", 2, 180.0);
		Menu m4 = new Menu(4, "chaap", 2, 190.0);
		Menu m5 = new Menu(5, "chillypotato", 1, 250.0);
		items.add(m1);
		items.add(m2);
		items.add(m3);
		items.add(m4);
		items.add(m5);

		System.out.println("Welcome to SURABHI Restaurant\n------------------------------------------------------------------");

		while (true) {
			System.out.println("Please  Enter Your Credentials");

			System.out.println("enter Email Id = ");
			String emailId = scanner.next();

			System.out.println("enter Password = ");
			String password = scanner.next();

			String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);

			System.out.println("Please Enter A if you are Admin and U if you are User, O to logout");
			String iWas = scanner.next();

			BillInformation bill = new BillInformation();
			List<Menu> OrderedItems = new ArrayList<Menu>();
			
			double totalCost = 0;
			
			Date date=new Date();
			
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy");
			String currentTime = time1.format(f);

			if (iWas.equals("U") || iWas.equals("u")) {

				System.out.println("Welcome Mr/Ms. " + name);
				do {
					System.out.println("Today's SURABHI's Menu :- ");
					items.stream().forEach(i -> System.out.println(i));
					System.out.println("Enter the Menu Item Code");
					int code = scanner.nextInt();
					
					if (code == 1) {
						OrderedItems.add(m1);
						totalCost += m1.getItemPrice();
					}

					else if (code == 2) {
						OrderedItems.add(m2);
						totalCost += m2.getItemPrice();
					}
					
					else if (code == 3) {
						OrderedItems.add(m3);
						totalCost += m3.getItemPrice();
					}
					
					else if (code == 4) {
						OrderedItems.add(m4);
						totalCost += m4.getItemPrice();
					}
					
					else {
						OrderedItems.add(m5);
						totalCost += m5.getItemPrice();
					}
					
					System.out.println("Press 0 to show totalBill \n Press 1 to order more");
					int choosen =scanner.nextInt();
					if (choosen == 0)
						finalOrder = false;
					else
						finalOrder = true;

				} while (finalOrder);
				

				System.out.println("Thanks Mr/Ms. " + name + " for dining in with Surabi :) ");
				System.out.println("Items you have Selected :-");
				OrderedItems.stream().forEach(e -> System.out.println(e));
				System.out.println("Your Total bill will be :- " + totalCost);

				bill.setUser(name);
				bill.setAmount(totalCost);
				bill.setItems(OrderedItems);
				bill.setTime(date);
				totalbill.add(bill);

			} else if (iWas.equals("A") || iWas.equals("a")) {
				System.out.println("Welcome Mr/Ms."+name+ ": Admin");
				System.out.println("Press 1 to see all the bills for today \n Press 2 to see all the bills for this month \n Press 3 to see all the bills");
				int AdmOpted = scanner.nextInt();
				switch ( AdmOpted) {
				case 1:
					if ( !totalbill.isEmpty()) {
						for (BillInformation b :totalbill) {
							if(b.getTime().getDate()==time.getDate()) {
							System.out.println("\n User :- " + b.getUser());
							System.out.println("Items ordered :- " + b.getItems());
							System.out.println("Total  Bill:- " + b.getAmount());
							System.out.println(" Visited on :- " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills today :( ");
					break;

				case 2:
					if (!totalbill.isEmpty()) {
						for (BillInformation b :totalbill) {
							if (b.getTime().getMonth()==time.getMonth()) {
							System.out.println("\nUser :- "+b.getUser());
							System.out.println("Items Ordered :- " + b.getItems());
							System.out.println("Total Bill:- "+b.getAmount());
							System.out.println("Visited on :- " + b.getTime() + "\n");
						}
						}
					} else
						System.out.println("No Bills for this month :(");
					break;

				case 3:
					if (!totalbill.isEmpty()) {
						for (BillInformation b :totalbill) {
							System.out.println("\nUser :-"+b.getUser());
							System.out.println("Items Ordered:- " + b.getItems());
							System.out.println("Total Bill :-"+b.getAmount());
							System.out.println("Visited on :-" + b.getTime() + "\n");
						}
					} else
						System.out.println("No Bills :(");

					break;

				default:
					System.out.println("Invalid Option :o ");
					System.exit(1);
				}
			} else if (iWas.equals("O") || iWas.equals("o")) {
				System.exit(1);
				
			}else  {
				System.out.println("Invalid Entry :o ");
			}

		}

	}
}


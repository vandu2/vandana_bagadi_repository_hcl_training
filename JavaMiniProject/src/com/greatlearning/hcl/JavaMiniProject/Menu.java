package com.greatlearning.hcl.JavaMiniProject;

public class Menu {
	private int itemID;
	private String itemName;
	private int itemQuantity;
	private double itemPrice;

	public Menu(int itemID, String itemname, int itemQuantity, double itemPrice) throws IllegalArgumentException{
		super();
	
		if(itemID<0)
		{
			throw new IllegalArgumentException("Exception raised, Select Correct Item Id");
		}
		this.itemID = itemID;
		
		if(itemname==null) {
			throw new IllegalArgumentException("Exception raised, name cannot ");

		}
		this.itemName = itemname;
		if(itemQuantity<0)
		{
			throw new IllegalArgumentException("Exception raised, quantity cannot less than zero");
		}
		
		this.itemQuantity = itemQuantity;
		
		if(itemPrice<0)
		{
			throw new IllegalArgumentException("Exception raised, price cannot less than zero");
		}
		this.itemPrice = itemPrice;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemname(String itemname) {
		this.itemName = itemname;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "" + itemID + " " + itemName + " " + itemQuantity + " "+ itemPrice  ;
	}
}

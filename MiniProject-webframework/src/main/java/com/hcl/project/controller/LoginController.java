package com.hcl.project.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


import com.hcl.project.dao.MenuRepo;
import com.hcl.project.entities.Menu;
import com.hcl.project.entities.User;
import com.hcl.project.service.UserService;

@Controller
public class LoginController {
	@Autowired
	private UserService userService;

	@Autowired
	MenuRepo menuRepo;

	@GetMapping("/login")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("user", new User());
		return mav;
	}

	@PostMapping("/login")
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("user") User user) {

		ModelAndView mav = null;

		User oauthUser = userService.login(user.getName(), user.getPassword());

		System.out.print(oauthUser);
		if (Objects.nonNull(oauthUser)) {

			mav = new ModelAndView("welcome");
			List<Menu> list = menuRepo.findAll();
			mav.addObject("list", list);
			mav.addObject("name", user.getName());
			mav.addObject("id", user.getId());

		} else {

			mav = new ModelAndView("login");
			mav.addObject("message", "Username or Password is wrong!");

		}
		return mav;
	}

}

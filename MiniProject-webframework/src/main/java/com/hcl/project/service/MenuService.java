package com.hcl.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.project.dao.MenuRepo;
import com.hcl.project.entities.Menu;


@Service
public class MenuService {
	
	@Autowired
	MenuRepo menuRepo;

	public List<Menu> viewmenu() {
		List<Menu> menu = menuRepo.findAll();
		return menu;
	}

	public void addMenu(Menu menu) {
		menuRepo.save(menu);
		
	}

	public void deleteMenu(Menu menu) {
		
		Optional <Menu> list =menuRepo.findById(menu.getItemid());
		
		if(list.isPresent()) {
			menuRepo.delete(list.get());
		}
		else {
			throw new RuntimeException("menu not found");
		}
	}
	
	
}



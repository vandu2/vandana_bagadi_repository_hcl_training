<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.hcl.project.entities.*"%>

<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">
<title>Profile Page</title>

</head>

<body>
	<div align=center>

		<%
		String name = (String) request.getAttribute("name");
		session.setAttribute("name", name);
		
		String menuName = (String) request.getAttribute("menuName");
		%>
		
		<%if(menuName!=null){ %>
		<h1 style="font-size: 15px"><%=menuName%>
			is deleted from Menu...
		</h1>
		<% }%>
		
		<h1 style="font-size: 80px">Welcome To Surabi</h1>
		
		<h1>Hello <%=name%></h1>
				
		<p>
			
			<br>
			<a href="showUsers?name=<%=name%>&amp" 
				style="font-size: 30px">Show Users </a>
			<br>
			<a href="addMenu?name=<%=name%>&amp" 
				style="font-size: 30px">Add Menu</a> 
			<br>	
			<a href="logout" style="font-size: 30px">Logout</a>
		</p>

		<div align=center>

			
              <table border="1" style="background-color: #BDB76B">

					<tr>
						<td>SR_NO</td>
						<td>Item_ID</td>
						<td>Item</td>
						<td>Total_Price</td>
						<td>Quantity</td>
						
					</tr>


					<%
					@SuppressWarnings("unchecked")
					List<Menu> menu = (List<Menu>) request.getAttribute("list");
					%>

					<%
					int i =1;
					for (Menu m : menu) {
					%>

					<tr>
						<td><%=i++%>
					     <td><%=m.getItemid()%></td>
					     <td><%=m.getItem()%></td>
					     <td><%=m.getTotal_price()%></td>
					     <td><%=m.getQuantity()%></td>
						
						
					</tr>
					<%
					}
					%>
			</table>
			
		</div>
	</div>
</body>
</html>
package com.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.User;
import com.service.UserService;

@RestController
@RequestMapping("/User")
public class UserController {
	@Autowired
	UserService userService;
	
	@GetMapping(value = "getAllUser",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUserInfo() {
		return userService.getAllUsers();
	}
	@PostMapping(value = "storeUser",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User use) {
		
				return userService.storeUserInfo(use);
	}
	@DeleteMapping(value = "deleteUser/{uid}")
	public String storeUserInfo(@PathVariable("uid") int uid) {
					return userService.deleteUserInfo(uid);
	}
	
	@PatchMapping(value = "updateUser")
	public String updateProductInfo(@RequestBody User use) {
					return userService.updateUserInfo(use);
	}


}



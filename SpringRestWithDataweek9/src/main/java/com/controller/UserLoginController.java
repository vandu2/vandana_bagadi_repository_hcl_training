package com.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.dao.Status;
import com.dao.UserLoginRepository;


import com.bean.UserLogin;
import javax.validation.Valid;
import java.util.List;

@RestController
public class UserLoginController {
	 @Autowired
	    UserLoginRepository userloginRepository;

	    @PostMapping("/userlogin/register")
	    public Status registerUser(@Valid @RequestBody UserLogin newUser) {
	        List<UserLogin> users = userloginRepository.findAll();

	        System.out.println("New user: " + newUser.toString());

	        for (UserLogin user : users) {
	            System.out.println("Registered user: " + newUser.toString());

	            if (user.equals(newUser)) {
	                System.out.println("User Already exists!");
	                return Status.USER_ALREADY_EXISTS;
	            }
	        }

	        userloginRepository.save(newUser);
	        return Status.SUCCESS;
	    }

	    @PostMapping("/userlogin/login")
	    public Status loginUser(@Valid @RequestBody UserLogin user) {
	        List<UserLogin> users = userloginRepository.findAll();

	        for (UserLogin other : users) {
	            if (other.equals(user)) {
	                user.setLoggedIn(true);
	                userloginRepository.save(user);
	                return Status.SUCCESS;
	            }
	        }

	        return Status.FAILURE;
	    }

	    @PostMapping("/userlogin/logout")
	    public Status logUserOut(@Valid @RequestBody UserLogin user) {
	        List<UserLogin> users = userloginRepository.findAll();

	        for (UserLogin other : users) {
	            if (other.equals(user)) {
	                user.setLoggedIn(false);
	                userloginRepository.save(user);
	                return Status.SUCCESS;
	            }
	        }

	        return Status.FAILURE;
	    }

	    @DeleteMapping("/userlogin/all")
	    public Status deleteUsers() {
	        userloginRepository.deleteAll();
	        return Status.SUCCESS;
	    }
	}


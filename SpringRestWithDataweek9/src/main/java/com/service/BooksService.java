package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BooksService {

	@Autowired
	BooksDao booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}
	
	public String storeBooksInfo(Books book) {
				
						if(booksDao.existsById(book.getBid())) {
									return "book id must be unique";
						}else {
									booksDao.save(book);
									return "book stored successfully";
						}
	}
	
	public String deleteBooksInfo(int bid) {
		if(!booksDao.existsById(bid)) {
			return "book details not present";
			}else {
			booksDao.deleteById(bid);
			return "book deleted successfully";
			}	
	}
	
	public String updateBooksInfo(Books book) {
		if(!booksDao.existsById(book.getBid())) {
			return "book details not present";
			}else {
			Books p	= booksDao.getById(book.getBid());	// if product not present it will give exception 
			p.setPrice(book.getPrice());						// existing product price change 
			booksDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "book updated successfully";
			}	
	}
	
}
